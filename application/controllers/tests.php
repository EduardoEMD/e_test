<?php
class tests extends CI_Controller{


	function all(){

		$data = array(
			'tests' =>	$this->dbf->find_all('test')
		);

		$this->layout->load('all',$data);
	}

	function test($id){


		$harcer = $this->dbf->find('question',array('test_id'=>$id));

		$tarberakner = $this->dbf->find('variant',array('test_id'=>$id));

		$data = array(

			'harcer' => $harcer,
			'tarberakner' => $tarberakner
		);

		$this->layout->load('test',$data);
	}
}