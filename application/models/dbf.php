<?php 
	class dbf extends CI_Model{
		
		function __construct(){

 			parent::__construct();

 		}

 		function insert($table,$data){

 			$this->db->insert($table,$data);
 		}

 		function update($id,$data,$table){

 			$this->db->where("id",$id);

 			$this->db->update($table,$data);
 			
 		}
 		function delete($id,$table){

 			$this->db->where("id",$id);

 			$this->db->delete($table);

 		}
 		function find($table,$data = array()){

 			$this->db->where($data);

 			$query = $this->db->get($table);

 			return $query->result_array();
 		}
 		function find_all($table,$sort = 'id', $order = 'asc'){

        	$this->db->order_by($sort, $order);

        	$query = $this->db->get($table);

       		return $query->result_array();

    	}
    	

	}