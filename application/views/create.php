<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		#container{
			width: 70%;
			height: auto;
			margin: auto;
		}
		#new_tester{
			width: 70%;
			height: auto;
			margin: auto;

		}
		.adns{
			margin-bottom: 10px;
		}
		.right-answer{
			background-color: rgba(119, 249, 43, 0.37)!important;
		}
	</style>
</head>
<body>
	<div id="container">
		<h4 id="message"></h4>
		<div class="row">
        	<div class="input-field col s6">
        	  <input id="test" type="text" class="validate">
        	  <label>Name of test</label>
        	  <button class="btn" id="questionadder">add question</button>
        	  <button class="btn" id="saver">Save</button>
        	</div>
        </div>
	</div>
	<div id="new_tester">
		
	</div>

</body>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.1/js/materialize.min.js"></script>
 <script type="text/javascript">
 	$(document).ready(function(){
 		$('#questionadder').click(function(){
 			var div = $("<div class='row'></div>")
 			$(div).append("<button class='btn adns'>Add answer</button>")
			$(div).append("<input class='quest' placeholder='add new question' style='background:rgba(177, 91, 156, 0.66)'>")
 			$('#new_tester').append(div)
 		})
 		$('#new_tester').on('click','.adns',function(){
 			$(this).after("<input class='validate answer' placeholder='type answer'>")
 		})

 		$('#new_tester').on('click','.answer',function(){
 			var x = $(this).parent().find('input')
 			$(x).each(function(){
 				if($(this).attr('class')!= 'quest'){
	 				$(this).attr('class','answer validate')
 				}
 			})
 			$(this).attr('class','right-answer')

 		})

 		$('#saver').click(function(){
 			var y = $('#new_tester').find('.row')
 			var data = []
 			var title = $('#test').val()
 			$(y).each(function(){
 				var right = $(this).find('.right-answer');
 				var variant = $(this).find('.answer');
 				var quest = $(this).find('.quest')
 				var error = "";
 				var empty = true
 				if(right.length!=1){
 					error="ճիշտ պատասխաններш քանակը սխալ է"
 					$('#message').css({'color':'darkred'})

 				}
 				if(title == ""){
 					error="անանուն թեստ"
 					$('#message').css({'color':'darkred'})
 				}
 				if(variant.length<1){
 					error="տարբերակները պետք է լինեն գոնե 1ից ավելի"
 					$('#message').css({'color':'darkred'})

 				}
 				if(quest[0].value ==""){
 					empty = false
 				}
 				$(right).each(function(){
 					if($(this).val()==""){
 						empty = false
 					}
 				})
 				$(variant).each(function(){
 					if($(this).val()==""){
 						empty = false
 					}
 				})
 				
 				if(empty == false){
 					error="չլրացված դաշտեր"
 					$('#message').css({'color':'darkred'})
 				}


 			
 				if(right.length==1 && variant.length>=1 && empty == true && title != ""){
 					error="ամեն ինչ լավ է"
 					$('#message').css({'color':'lightgreen'})
 					var obj = {}
 					obj.harc = $(quest[0]).val()
 					obj.pat = $(right[0]).val()
 					obj.tarb = []
 					for( i = 0; i < variant.length ; i++){
 						obj.tarb.push($(variant[i]).val())
 					}
 					data.push(obj)
 					
 				}
 				$('#message').html(error)
 				empty = true

 			})

 			
 			if($('#message').html() == "ամեն ինչ լավ է"){

				$.ajax({

					asyns:'false',
					type:'post',
					url:"<?= base_url('admin/save') ?>",
					data:{'newTest':data,"title":title},
					dataType:'json',
					success:function(r){
						console.log(r)
					}
				})
 			}
 		})




 	})

 </script>
</html>