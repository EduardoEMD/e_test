<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title></title>
	<style>
		h4{
			display: inline-block;
		}
		.input-field{
			width: 50%;
			margin: 0 auto;
			margin-bottom: 5%;
			
		}
		.container{
			width: 80%;
			height: auto;
			margin: 0 auto;
			text-align: center;
		}
	</style>
</head>
<body>
	<div class="container">
		<?php foreach ($harcer as $harc): ?>
			<h4><?= $harc['name']; ?></h4><br>
			<div class="input-field col s6">
				<select class="quest">
				<?php foreach ($tarberakner as $tarberak): ?>
			
						<?php if($tarberak['question_id'] == $harc['id']): ?>
								<option value="<?= $tarberak['status']; ?>"><?= $tarberak['tarberak']; ?></option>
						<?php endif; ?>

				<?php endforeach; ?>
				</select>
					
			</div>
		<?php endforeach; ?>
		<button class="btn done">Done</button>
		<h3 style="color:lightgreen" id="rig"></h3>
		<h3 style="color:orangered" id="fal"></h3>
	</div>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.1/js/materialize.min.js"></script>
 <script>
 	$(document).ready(function() {
   		$('select').material_select();
   		$('.done').click(function(){
   			var right = 0,falsh = 0;
   			$('.quest option:selected').each(function(){
   				if($(this).val() == 'true'){
   					right++
   				}
   				if($(this).val() == 'false'){
   					falsh++
   				}

   			})
   				$('#rig').html("Ճիշտ պատասխանների քանակ` "+right)
   				$('#fal').html("Սխալ պատասխանների քանակ` "+falsh)
   				
   		})

  });
 </script>
</html>