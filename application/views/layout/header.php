<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.1/css/materialize.min.css">

</head>
<body>
	 <nav>
    <div class="nav-wrapper">
      <a href="#" class="brand-logo">Logo</a>
      <ul id="nav-mobile" class="right hide-on-med-and-down">
        <li><a href="<?= base_url('admin/create'); ?>">Create</a></li>
        <li><a href="<?= base_url('tests/all'); ?>">all tests</a></li>
      </ul>
    </div>
  </nav>
</body>
</html>