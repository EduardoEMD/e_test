<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>	</title>
	<style>
		table{
			width: 56%;
			height: auto;
			margin:0 auto;
			margin-top: 10%;

		}
		td,th{
			text-align: center;
			width: 10%;
		}
	
	</style>
</head>
<body>
	<table class="striped">
		<tr>
			<th>id</th>
			<th>name</th>
			<th class="butik">edit</th>
		</tr>
		<?php foreach($tests as $test): ?>
			<tr>
				<td><?= $test['id'] ?></td>
				<td><?= $test['name'] ?></td>
				<td class="butik"><a href=<?= base_url("tests/test/").$test['id'] ?>><button class="btn">Edit</button></a></td>
			</tr>
		<?php endforeach; ?>
	</table>
		
</body>

</html>